# pyOgsProject
Scripts to generate, read and manipulate vtu grid files.

# License and copyright

Copyright (c) 2015-2018, OpenGeoSys Community (http://www.opengeosys.org) Distributed under a Modified MIT License. See accompanying file LICENSE.txt or http://www.opengeosys.org/project/license.
