#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import vtk as vtk
import numpy as np


class MeshPointFinder:
    ## Class which contains functionalities to find points within a given mesh
    # according to given criteria.
    # @VAR mesh: mesh on which points are located
    # @date: 2018 - Today
    # @author: jasper.bathmann@ufz.de
    def dummyZFunction(x,y):
        return 0*x*y

    def __init__(self, mesh, z=dummyZFunction):
        self.mesh = mesh
        self.z = z

    def findClosestPoint(self, point):
        Loc = vtk.vtkPointLocator()
        Loc.SetDataSet(self.mesh)

        Id = Loc.FindClosestPoint(point)
        return Id, self.mesh.GetPoints().GetPoint(Id)

    def findPointsWithinRadius(self, point, radius):
        Loc = vtk.vtkPointLocator()
        Loc.SetDataSet(self.mesh)
        points = []
        ids = []
        iDs = vtk.vtkIdList()
        Loc.FindPointsWithinRadius(radius, point, iDs)
        for i in range(iDs.GetNumberOfIds()):
            points.append(self.mesh.GetPoints().GetPoint(iDs.GetId(i)))
            bulk_node_ids = self.mesh.GetPointData().GetArray("bulk_node_ids")
            ids.append(int(bulk_node_ids.GetTuple(iDs.GetId(i))[0]))
        if len(points) < 4:
            points = []
            ids = []
            iDs = vtk.vtkIdList()
            Loc.FindClosestNPoints(4, point, iDs)
            for i in range(iDs.GetNumberOfIds()):
                points.append(self.mesh.GetPoints().GetPoint(iDs.GetId(i)))
                bulk_node_ids = self.mesh.GetPointData().GetArray(
                        "bulk_node_ids")
                ids.append(int(bulk_node_ids.GetTuple(iDs.GetId(i))[0]))
        return points, ids

    def findPointsOnPlane(self, nx, ny, nz, ox, oy, oz):
        points = []
        ids = []
        bounds = (self.mesh.GetBounds())
        epsilon_x = bounds[1] - bounds[0]
        epsilon_y = bounds[3] - bounds[2]
        epsilon_z = bounds[3] - bounds[2]
        if nx == 1:
            epsilon_x = 2e-5
        if ny == 1:
            epsilon_y = 2e-5
        if nz == 1:
            epsilon_z = 2e-5
        n_points = self.mesh.GetNumberOfPoints()
        for iD in range(n_points):
            point = self.mesh.GetPoints().GetPoint(iD)
            bed_x = (np.abs(point[0] - ox) <= epsilon_x)
            bed_y = (np.abs(point[1] - oy) <= epsilon_y)
            bed_z = (np.abs(point[2] - self.z(point[0],point[1])) <= epsilon_z)
            if(bed_x and bed_y and bed_z):
                    points.append(self.mesh.GetPoints().GetPoint(iD))
                    ids.append(iD)
        return points, ids

    def extractListNegative(self, pointlist):
        meshpoints = self.mesh.GetPoints()
        nmeshpoints = meshpoints.GetNumberOfPoints()
        iDs = []
        ids = []
        for point in pointlist:
            iDs.append(point[0])
        points = []
        for iD in range(nmeshpoints):
            if(iD in iDs):
                iDs.remove(iD)
            elif(meshpoints.GetPoint(iD)[2] == 0):
                points.append(meshpoints.GetPoint(iD))
                ids. append(iD)
        return points
