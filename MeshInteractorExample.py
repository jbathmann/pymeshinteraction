#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@date: 2018-Today
@author: jasper.bathmann@ufz.de
"""

import MeshInteractor as MI
import numpy as np

# This is an short example how to create an delaunay triangulation from a set
# of points and how to calculate the areas associated to this using the
# MeshInterActor class
N = 30
points = np.concatenate((np.random.rand(N,2),np.zeros((N,1))),axis=1)
center = (np.array([points[:,0].sum(),points[:,1].sum(),0])).reshape((1,3))/float(N)

print(points)
ids = np.arange(N)
mesh = MI.MeshInteractor("area_tester")
mesh.CreateMeshFromPoints([points,ids])
mesh.CreateMultipleTriangles()
mesh.getMeshAreaForTriangles()
#mesh.OutputMesh("TestMesh")
