#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import vtk as vtk
import numpy as np
import matplotlib as mpl


class MeshEditor:
    ## This class is currently not used. Most functionalities given here,
    # are also implemented in the MeshInteractor class.
    # @date: 2018 - Today
    # @author: jasper.bathmann@ufz.de

    def __init__(self, mesh):
        self.mesh = mesh

    def AddPropertyVector(self, values, name, tYpe):
        if tYpe == "double":
            propertyvector = vtk.vtkDoubleArray()
        propertyvector.SetName(name)
        for value in values:
            propertyvector.InsertNextTuple1(value)

        self.mesh.GetPointData().AddArray(propertyvector)

    def AddConstantToPropertyVector(self, vector, constant):
        n_values = vector.GetNumberOfTuples()
        for i in range(n_values):
            tuplei = vector.GetComponent(i, 0) + constant
            vector.SetComponent(i, 0, tuplei)

    def AddHomogenousPropertyVector(self, value, name, tYpe):
        if tYpe == "double":
            propertyvector = vtk.vtkDoubleArray()
        propertyvector.SetName(name)
        npoints = self.mesh.GetPoints().GetNumberOfPoints()
        for i in range(npoints):
            propertyvector.InsertNextTuple1(value)
        self.mesh.GetPointData().AddArray(propertyvector)

    def AddHomogenousPropertyVectorInRegion(
            self, bedxl, bedxr, bedyl, bedyr, bedzl, bedzr, name, tYpe, value):
        if tYpe == "double":
            propertyvector = vtk.vtkDoubleArray()
        propertyvector.SetName(name)
        points = self.mesh.GetPoints()
        npoints = points.GetNumberOfPoints()
        for i in range(npoints):
            point = points.GetPoint(i)
            xbool = bedxl <= point[0] <= bedxr
            ybool = bedyl <= point[1] <= bedyr
            zbool = bedzl <= point[2] <= bedzr
            if(xbool and ybool and zbool):
                propertyvector.InsertNextTuple1(value)
            else:
                propertyvector.InsertNextTuple1(0)
        self.mesh.GetPointData().AddArray(propertyvector)

    def CreateTriangle(self):
        cells = vtk.vtkCellArray()
        cell = vtk.vtkTriangle()
        Ids = cell.GetPointIds()
        for kId in range(3):
            Ids.SetId(kId, kId)
        cells.InsertNextCell(cell)
        self.mesh.SetCells(5, cells)

    def CreateQuad(self):
        cells = vtk.vtkCellArray()
        cell = vtk.vtkQuad()
        Ids = cell.GetPointIds()
        for kId in range(4):
            Ids.SetId(kId, kId)
        cells.InsertNextCell(cell)
        self.mesh.SetCells(9, cells)

    def CreateMultipleTriangles(self):
        cells = vtk.vtkCellArray()
        cell = vtk.vtkQuad()
        Ids = cell.GetPointIds()
        points = self.mesh.GetPoints()
        bounds = points.GetBounds()
        shiftx = (bounds[0]+bounds[1])/2.
        shifty = (bounds[2]+bounds[3])/2.
        npoints = points.GetNumberOfPoints()
        pointsx = np.empty(npoints)
        pointsy = np.empty(npoints)
        for point in range(npoints):
            pointsx[point] = points.GetPoint(point)[0] - shiftx
            pointsy[point] = points.GetPoint(point)[1] - shifty
        triang = mpl.tri.Triangulation(pointsx, pointsy)
        pointids = triang.triangles
        for ids in pointids:
            cell = vtk.vtkTriangle()
            Ids = cell.GetPointIds()
            for kId in range(3):
                Ids.SetId(kId, ids[kId])
            cells.InsertNextCell(cell)
        self.mesh.SetCells(5, cells)

    def CreateCellProperty(self, PropertyName, value, numberOfCells):
        propertyvector = vtk.vtkDoubleArray()
        propertyvector.SetNumberOfComponents(4)
        propertyvector.SetName(PropertyName)
        if(type(value) == np.ndarray):
            for PointId in range(numberOfCells):
                propertyvector.InsertNextTuple4(
                        value[PointId], 0, 0, value[PointId])
        else:
            for PointId in range(numberOfCells):
                propertyvector.InsertNextTuple1(value)
        self.mesh.GetCellData().AddArray(propertyvector)
