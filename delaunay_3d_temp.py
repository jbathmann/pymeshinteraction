#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@date: 2018-Today
@author: jasper.bathmann@ufz.de
"""

    def create3DDelaunayGrid(
            self, extension_points_X, originX, lengthX, extension_points_Y,
            originY, lengthY, extension_points_Z, originZ, lengthZ):
        self.extension_points_X = extension_points_X
        self.extension_points_Y = extension_points_Y
        self.extension_points_Z = extension_points_Z
        self.lengthX = lengthX
        self.lengthY = lengthY
        self.lengthZ = lengthZ
        self.ox = originX
        self.oy = originY
        self.oz = originZ
        self.celltypes = []
        self.grid = vtk.vtkUnstructuredGrid()

        self.points = vtk.vtkPoints()
        self.cells = vtk.vtkCellArray()
        self.dx = self.lengthX/float(self.extension_points_X-1)
        self.dy = self.lengthY/float(self.extension_points_Y-1)
        self.dz = self.lengthZ/float(self.extension_points_Z-1)
        Nz = float(self.extension_points_Z-1)
        factor = 5.
        grundlaenge = (2 * (1 - Nz) / Nz * self.lengthZ
                       / (factor - Nz - Nz * factor + 1))

        def laenge(i, grundlaenge=grundlaenge, Nz=Nz, factor=factor):
            return (grundlaenge
                    / (1 - Nz) * ((factor - 1) * i + 1 - factor * Nz))
        self.dz = laenge(i=1)
        for pointIdY in range(1, self.extension_points_Y-1):
            coordy = pointIdY * self.dy + self.oy

            self.points.InsertNextPoint(self.ox, coordy, self.oz)
            self.points.InsertNextPoint(self.ox + self.lengthX, coordy,
                                        self.oz)
            self.points.InsertNextPoint(self.ox, coordy, self.oz + self.dz)
            self.points.InsertNextPoint(self.ox + self.lengthX, coordy,
                                        self.oz + self.dz)

        for pointIdX in range(self.extension_points_X):
            coordx = pointIdX * self.dx + self.ox
            self.points.InsertNextPoint(coordx, self.oy, self.oz)
            self.points.InsertNextPoint(coordx, self.oy + self.lengthY,
                                        self.oz)
            self.points.InsertNextPoint(coordx, self.oy, self.oz + self.dz)
            self.points.InsertNextPoint(coordx, self.oy + self.lengthY,
                                        self.oz + self.dz)

        for pointIdY in range(1, self.extension_points_Y-1):
            for pointIdX in range(1, self.extension_points_X-1):
                dz = self.dz
                coordx = ((pointIdX + np.random.uniform(-.25, .25)) * self.dx
                          + self.ox)
                coordy = ((pointIdY + np.random.uniform(-.25, .25)) * self.dy
                          + self.oy)
                self.points.InsertNextPoint(coordx, coordy, self.oz)
                self.points.InsertNextPoint(coordx, coordy, self.oz + dz)
        z = self.oz + self.dz
        self.grid.SetPoints(self.points)
        self.create3DTriangles()
        gridlist = []
        for i in range(2, self.extension_points_Z):
            dz = laenge(i)

            gridlist.append(self.shiftGridByZCoordinate(z, self.grid, dz))
            z += dz

        self.CombineMultipleGrids(gridlist)