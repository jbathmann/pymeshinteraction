#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from __future__ import absolute_import

import vtk as vtk
import numpy as np
import pygmsh
import scipy.spatial as spatial


class MeshInteractor:
    ## Main tool to create and edit vtu grid files. Here, two and three
    #  dimensional rectangular grids can be created. Additionally regular grids
    #  can be filled with cells and delaunay is available for 2 and 3
    #  dimensional grids. Build in functions to create and remove heteregenous
    #  property vectors are implemented as well. Additionally a function
    #  combining multiple 2 dimensional grids using delaunay to generate the
    #  new cell distribution is implemented.
    #  @date: 2018 - Today
    #  @author: jasper.bathmann@ufz.de
    def dummyZFunction(x, y):
        return 0 * x * y

    def dummyGrundlaenge(obj):
        return (2 * (1 - obj.z_layers) / obj.z_layers * obj.length_z /
                (obj.factor_z - obj.z_layers -
                 obj.z_layers * obj.factor_z + 1))

    def dummyZOffsetLaenge(i, grundlaenge, Nz, factor):
        return (grundlaenge
                / (1 - Nz) * ((factor - 1) * i + 1 - factor * Nz))

    def __init__(self, meshName, z=dummyZFunction):
        self.meshName = meshName
        self.grid = vtk.vtkUnstructuredGrid()
        self.points = vtk.vtkPoints()
        self.cells = vtk.vtkCellArray()
        self.pilot_points = []
        self.z = z

    def setMeshGeometry(self, extension_points_X, originX, lengthX,
                        extension_points_Y, originY, lengthY,
                        z=dummyZFunction, z_layers=5, lengthZ=5,
                        factor_z=5, base_length_z=dummyGrundlaenge):
        self.extension_points_X = extension_points_X
        self.extension_points_Y = extension_points_Y
        self.extension_points_Z = z_layers + 1
        self.z_layers = z_layers
        self.factor_z = factor_z
        self.length_x = lengthX
        self.length_y = lengthY
        self.length_z = lengthZ
        self.ox = originX
        self.oy = originY
        self.oz = z(self.ox, self.oy)
        self.z = z
        self.thickness_first_layer = base_length_z(self)

    def createMesh(self):
        self.addFirstLayer()
        self.addMoreLayers()
        self.addNodeIds()

    def updateMesh(self):
        self.temp_grid = self.grid
        self.grid = vtk.vtkUnstructuredGrid()
        self.points = vtk.vtkPoints()
        self.cells = vtk.vtkCellArray()

        self.addFirstLayer()
        self.addMoreLayers()
        self.resampleDataset()
        self.addNodeIds()

    def addNodeIds(self):
        self.node_ids = vtk.vtkDataArray.CreateDataArray(
                vtk.VTK_UNSIGNED_LONG)
        self.node_ids.SetName("bulk_node_ids")
        self.surface = MeshInteractor("surface")
        k = 0
        j = 0
        self.surface_points = []
        self.surface_ids = []
        for i in range(self.grid.GetNumberOfPoints()):
            self.node_ids.InsertNextTuple1(i)
            point = self.grid.GetPoints().GetPoint(i)
            if np.abs(point[2]-self.z(point[0],point[1]))<1e-5:
                self.surface_points.append(point)
                self.surface_ids.append(i)

        self.surface.CreateMeshFromPoints(
                [self.surface_points, self.surface_ids])
        self.grid.GetPointData().AddArray(self.node_ids)

    def resetMesh(self):
        self.grid = vtk.vtkUnstructuredGrid()
        self.points = vtk.vtkPoints()
        self.cells = vtk.vtkCellArray()
        self.pilot_points = []

    def addFirstLayer(self):
        self.surface_points = []
        self.addSurfacePointPygmsh()

        self.surface = MeshInteractor("surface")
        self.surface.CreateMeshFromPoints(
                [self.surface_points, np.arange(len(self.surface_points))])

    def addMoreLayers(self, offsets=dummyZOffsetLaenge):
        z = self.thickness_first_layer
        gridlist = []
        for i in range(1, self.z_layers)[::-1]:

            dz = offsets(i, self.thickness_first_layer,
                         self.z_layers, self.factor_z)
            gridlist.append(self.shiftGridByZCoordinate(z, self.grid, dz))
            z += dz

        self.combineMultipleGrids(gridlist)

    def shiftGridByZCoordinate(self, z,  grid, dz):
        points = grid.GetPoints()
        point_number = points.GetNumberOfPoints()
        cells = grid.GetCells()
        new_grid = vtk.vtkUnstructuredGrid()
        new_points = vtk.vtkPoints()
        for i in range(point_number):
            point_coords = points.GetPoint(i)
            if np.abs(point_coords[2] -
                      self.z(point_coords[0], point_coords[1])) < 1e-5:
                shiftz = point_coords[2] - z
            else:
                shiftz = point_coords[2] - z - dz + self.thickness_first_layer
            new_points.InsertNextPoint(point_coords[0], point_coords[1],
                                       shiftz)
        new_grid.SetPoints(new_points)
        new_grid.SetCells(10, cells)
        return new_grid

    def addPilotPoint(self, pilot_point):
        self.pilot_points.append(pilot_point)

    def identifyTreeGroups(self):
        pairs = np.array([], dtype=int).reshape(0, 2)
        for pilot_point1, i in zip(self.pilot_points,
                                   range(len(self.pilot_points))):
            for pilot_point2, j in zip(
                    self.pilot_points[(i+1):],
                    i + 1 + np.arange(len(self.pilot_points[(i+1):]))):
                distance = spatial.distance.cdist(
                        pilot_point1.midpoint,
                        pilot_point2.midpoint)
                if(distance < pilot_point1.r+pilot_point2.r
                   and (distance > (pilot_point1.r - 2 * pilot_point2.r) or
                        distance > (pilot_point2.r - 2 * pilot_point1.r))):

                    pair = np.array([[i, j]])
                    pairs = np.concatenate((pairs, pair), axis=0)
        groups = []
        for pair, i in zip(pairs, range(len(pairs))):
            found1 = False
            found0 = False
            for group in groups:
                if pair[0] in group:
                    found0 = True
                if pair[1] in group:
                    found1 = True
                if found0 or found1:
                    if (found0 and not found1):
                        group.append(pair[1])
                    if (found1 and not found0):
                        group.append(pair[0])
            if not (found1 or found0):
                groups.append([pair[0], pair[1]])
        return groups

    def addSurfacePointPygmsh(self):
        self.grid = vtk.vtkUnstructuredGrid()
        self.points = vtk.vtkPoints()
        self.cells = vtk.vtkCellArray()
        N = float(self.extension_points_Y-1) * float(self.extension_points_X-1)
        char_length = np.sqrt(self.length_x*self.length_y/N)

        geom = pygmsh.opencascade.Geometry(
                characteristic_length_min=.02*char_length,
                characteristic_length_max=char_length)
        domain = geom.add_rectangle(
                [self.ox, self.oy, 0.0], self.length_x, self.length_y,
                corner_radius=0, char_length=2
            )
        groups = self.identifyTreeGroups()
        id_in_group = []
        for group in groups:
            for iD in group:
                id_in_group.append(iD)
        id_in_group = np.array(id_in_group)
        used = []
        for pilot_point, i in zip(self.pilot_points,
                                  range(len(self.pilot_points))):
            used_array = np.array(used)
            if(i in id_in_group and i not in used_array):
                for group in groups:
                    points = np.array([]).reshape(0, 2)
                    if i in group:
                        for iD in group:
                            used.append(iD)

                            new_points = self.pilot_points[iD].sampleSphere()
                            points = np.concatenate((new_points, points))
                        hull = spatial.ConvexHull(points)
                        geom_points = []
                        for vert in hull.vertices:
                            geom_points.append(
                                    geom.add_point(
                                            [points[vert][0],
                                             points[vert][1],
                                             0],
                                            lcar=char_length))
                        lines = []
                        for k in range(len(geom_points)-1):
                            lines.append(
                                    geom.add_line(geom_points[k],
                                                  geom_points[k+1])
                                    )
                        lines.append(
                                geom.add_line(
                                        geom_points[-1],
                                        geom_points[0]))
                        line_loop = geom.add_line_loop(lines)
                        surface = geom.add_plane_surface(line_loop)
                        domain = geom.boolean_union(
                                [domain, surface])
            elif(i not in used_array):
                point = geom.add_disk(
                        pilot_point.midpoint[0],
                        pilot_point.fac_r[0] * pilot_point.r,
                        char_length=pilot_point.fac_char[0] * pilot_point.r)
                domain = geom.boolean_union([domain, point])
        geom.extrude(
                domain, [0, 0, -self.thickness_first_layer], num_layers=1)
        mesh = pygmsh.generate_mesh(
                geom, mesh_file_type="vtk", remove_faces=True)

        for cell_point_ids in mesh.cells["tetra"]:
            cell = vtk.vtkTetra()
            for i in range(4):
                cell.GetPointIds().SetId(i, cell_point_ids[i])
            self.cells.InsertNextCell(cell)

        for point in mesh.points:
            self.points.InsertNextPoint(
                    point[0], point[1], self.z(point[0], point[1])+point[2])
            if point[2] == 0:
                self.surface_points.append(point)
        self.grid.SetPoints(self.points)
        self.grid.SetCells(10, self.cells)

    def addSurfacePointGrid(self):
        if self.extension_points_X == 1:
            self.dx = 0
        else:
            self.dx = self.length_x/float(self.extension_points_X-1)
        if self.extension_points_Y == 1:
            self.dy = 0
        else:
            self.dy = self.length_y/float(self.extension_points_Y-1)
        for pointIdY in range(self.extension_points_Y):
            for pointIdX in range(self.extension_points_X):
                coordx = pointIdX * self.dx + self.ox
                coordy = pointIdY * self.dy + self.oy
                coordz = self.z(coordx, coordy)
                self.points.InsertNextPoint(coordx, coordy, coordz)

    def shiftPointsByZCoordinate(self, points, dz):
        N = self.points.GetNumberOfPoints()
        for i in range(N):
            point = self.points.GetPoint(i)
            coord_z_shifted = point[2] - dz
            self.points.InsertNextPoint(point[0], point[1], coord_z_shifted)

    def combineMultipleGrids(self, gridlist, mergetolerance=1e-6):
        mainGrid = vtk.vtkUnstructuredGrid()
        appendFilter = vtk.vtkAppendFilter()
        appendFilter.AddInputData(self.grid)
        for i in range(len(gridlist)):
            appendFilter.AddInputData(gridlist[i])
        appendFilter.Update()
        newGrid = vtk.vtkUnstructuredGrid()
        newGrid.ShallowCopy(appendFilter.GetOutput())
        cellmerge = vtk.vtkMergeCells()
        cellmerge.MergeDuplicatePointsOn()
        cellmerge.SetTotalNumberOfCells(newGrid.GetNumberOfCells())
        cellmerge.SetTotalNumberOfPoints(newGrid.GetNumberOfPoints())
        cellmerge.SetTotalNumberOfDataSets(1)
        cellmerge.SetPointMergeTolerance(mergetolerance)
        cellmerge.SetUnstructuredGrid(mainGrid)
        cellmerge.MergeDataSet(newGrid)
        cellmerge.Finish()

        self.grid = cellmerge.GetUnstructuredGrid()

    def CreateMeshFromPoints(self, points):
        self.grid = vtk.vtkUnstructuredGrid()
        self.points = vtk.vtkPoints()
        propertyvector = vtk.vtkDataArray.CreateDataArray(
                vtk.VTK_UNSIGNED_LONG)
        propertyvector.SetName("bulk_node_ids")
        for i in range(len(points[0])):
            point = points[0][i]
            iD = points[1][i]
            self.points.InsertNextPoint(point + np.array([0,0,self.z(point[0],point[1])]))
            propertyvector.InsertNextTuple1(iD)
        self.grid.SetPoints(self.points)
        self.grid.GetPointData().AddArray(propertyvector)

    def CreateMultipleTriangles(self):
        if(self.grid.GetNumberOfPoints() == 1):
            cells = vtk.vtkCellArray()
            cell = vtk.vtkVertex()
            Ids = cell.GetPointIds()
            Ids.SetId(0, 0)
            cells.InsertNextCell(cell)
            self.grid.SetCells(1, cells)
        if(self.grid.GetNumberOfPoints() == 2):
            cells = vtk.vtkCellArray()
            cell = vtk.vtkLine()
            Ids = cell.GetPointIds()
            for kId in range(2):
                Ids.SetId(kId, kId)
            cells.InsertNextCell(cell)
            self.grid.SetCells(3, cells)
        else:
            delaunay = vtk.vtkDelaunay2D()
            delaunay.SetInputData(self.grid)
            delaunay.Update()
            cells = delaunay.GetOutput().GetPolys()

            self.grid.SetCells(5, cells)

    def getMeshAreaForTriangles(self):
        cells = self.grid.GetCells()
        points = self.grid.GetPoints()
        total_area = 0
        pts = vtk.vtkIdList()
        cells.InitTraversal()
        N_points = points.GetNumberOfPoints()
        nodal_areas = np.zeros(N_points)
        for i in range(cells.GetNumberOfCells()):
            cells.GetNextCell(pts)
            id0_o = pts.GetId(0)
            id1_o = pts.GetId(1)
            id2_o = pts.GetId(2)
            p0_o = np.array(points.GetPoint(id0_o))
            p1_o = np.array(points.GetPoint(id1_o))
            p2_o = np.array(points.GetPoint(id2_o))
            midpoint_b = (p0_o+p2_o)/2
            a_to_midpoint_b = midpoint_b - p0_o
            a_to_midpoint_b_perp = np.array([a_to_midpoint_b[1],
                                             -a_to_midpoint_b[0], 0])
            midpoint_c = (p0_o + p1_o)/2.
            a_to_midpoint_c = midpoint_c - p0_o
            a_to_midpoint_c_perp = np.array([a_to_midpoint_c[1],
                                             -a_to_midpoint_c[0], 0])
            diff_midpoints = midpoint_b - midpoint_c
            tau = ((diff_midpoints[0] * a_to_midpoint_c_perp[1] -
                    diff_midpoints[1] * a_to_midpoint_c_perp[0]) /
                   (a_to_midpoint_b_perp[0] * a_to_midpoint_c_perp[1] -
                    a_to_midpoint_b_perp[1] * a_to_midpoint_c_perp[0]))
            circ_center = midpoint_b - a_to_midpoint_b_perp * tau
            for j in range(3):
                id0 = pts.GetId((0 + j) % 3)
                id1 = pts.GetId((1 + j) % 3)
                id2 = pts.GetId((2 + j) % 3)
                p0 = np.array(points.GetPoint(id0))
                p1 = np.array(points.GetPoint(id1))
                p2 = np.array(points.GetPoint(id2))
                v_as = circ_center - p0
                v_ab = p1 - p0
                v_ac = p2 - p0
                sign = np.sign(v_ab[0]*v_ac[1]-v_ab[1]*v_ac[0])
                norm_v_ab = (v_ab[0]**2 + v_ab[1]**2)**.5
                norm_v_ac = (v_ac[0]**2 + v_ac[1]**2)**.5
                v_abg = v_ab * np.dot(v_as, v_ab) / norm_v_ab**2
                v_acg = v_ac * np.dot(v_ac, v_as) / norm_v_ac**2
                A1 = -(v_as[0]*v_abg[1]-v_as[1]*v_abg[0])/2.
                A2 = (v_as[0]*v_acg[1]-v_as[1]*v_acg[0])/2.
                nodal_areas[id0] += sign * (A1 + A2)
        self.RemovePropertyVector("nodal_areas")
        self.addPropertyVector(nodal_areas, "nodal_areas",
                               "double")
        total_area = nodal_areas.sum()
        return total_area

    def create3DTriangles(self):
        delaunay = vtk.vtkDelaunay3D()
        delaunay.SetInputData(self.grid)
        delaunay.BoundingTriangulationOff()
        delaunay.Update()

        self.cells = delaunay.GetOutput().GetCells()
        self.points = delaunay.GetOutput().GetPoints()
        self.grid = delaunay.GetOutput()

    def setDxDyDz(self, dx, dy, dz):
        self.dx, self.dy, self.dz = dx, dy, dz

    def addPropertyVector(self, values, name, tYpe):
        values = values * np.ones(self.grid.GetPoints().GetNumberOfPoints())
        if tYpe == "double":
            propertyvector = vtk.vtkDoubleArray()
        if tYpe == "unsigned_long":
            propertyvector = vtk.vtkDataArray.CreateDataArray(
                    vtk.VTK_UNSIGNED_LONG)
            values = np.array(
                    values, dtype=np.unsignedinteger)
        propertyvector.SetName(name)
        for value in values:
            propertyvector.InsertNextTuple1(value)

        self.grid.GetPointData().AddArray(propertyvector)

    def RemovePropertyVector(self, name):
        self.grid.GetPointData().RemoveArray(name)

    def getPropertyVector(self, name):
        prop = self.grid.GetPointData().GetArray(name)
        value_number = self.grid.GetPoints().GetNumberOfPoints()
        value_array = np.empty(value_number, dtype=float)
        for i in range(value_number):
            value_array[i] = prop.GetTuple(i)[0]
        return value_array

    def getPropertyVectorAtNodeIds(self, name, node_ids):
        prop = self.grid.GetPointData().GetArray(name)
        value_array = np.empty(len(node_ids), dtype=float)
        for i in range(len(node_ids)):
            value_array[i] = prop.GetTuple(int(node_ids[i]))[0]
        return value_array

    def readMesh(self, inputdir, meshName=""):
        if meshName == "":
            self.temp_name = self.meshName
        else:
            self.temp_name = meshName
        self.meshReader = vtk.vtkXMLUnstructuredGridReader()
        self.meshReader.SetFileName(inputdir+self.temp_name+".vtu")
        self.meshReader.Update()
        self.temp_grid = self.meshReader.GetOutput()
        self.temp_points = self.grid.GetPointData()
        self.temp_cells = self.grid.GetCells()

        return self.temp_grid.GetCellData()

    def resampleDataset(self):
        resample_filter = vtk.vtkResampleWithDataSet()
        resample_filter.SetSourceData(self.temp_grid)
        resample_filter.SetInputData(self.grid)
        resample_filter.Update()
        self.grid = resample_filter.GetOutput()


    def setTempMeshAsMainMesh(self):
        self.grid = self.temp_grid
        self.points = self.temp_points
        self.cells = self.temp_cells

    def outputMesh(self, outputdir):

        writer = vtk.vtkXMLUnstructuredGridWriter()
        writer.SetFileName(outputdir + self.meshName + ".vtu")
        writer.SetInputData(self.grid)
        writer.Write()
