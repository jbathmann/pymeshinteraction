#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@date: 2018-Today
@author: jasper.bathmann@ufz.de
"""
import numpy as np
from . import MeshInteractor
from . import MeshPointFinder as MPF
import pygmsh


class PilotPoint:

    def dummyZFunction(x, y):
        return 0 * x * y

    def __init__(self, r, global_mesh,
                 x=0, y=0, fac_r=[.999,2], fac_char=[0.1,2.5], z=dummyZFunction):
        self.z = z
        self.midpoint = np.array([[x, y, z(x, y)]])
        self.fac_r = fac_r
        self.fac_char = fac_char
        self.r = r
        self.n_points = round(4 * np.pi/(fac_char[0]))

        self.parameter_names = []
        self.parameter_values = []
        self.parameter_area_weighted = []
        self.global_mesh = global_mesh

    def updateRadius(self, r):
        self.r = r

    def setPosition(self, x, y):
        self.midpoint = np.array([[x, y, self.z(x, y)]])

    def updateCorrespondingPoints(self, bounding_box):
        all_points = self.sampleSphere()
        self.corresponding_points = self.pointInBox(all_points, bounding_box)

    def giveCorrespondingPoints(self):
        return self.corresponding_points

    def pointInBox(self, points, box):
        j = 0
        for point in points:
            outside = False
            if box[0] > point[0]:
                point[0] = box[0]
                outside = True
            if box[1] < point[0]:
                point[0] = box[1]
                outside = True
            if box[2] > point[1]:
                point[1] = box[2]
                outside = True
            if box[3] < point[1]:
                point[1] = box[3]
                outside = True
            if outside:
                j += 1
        if j > len(points)*(2/3):
            return np.array([])
        return points

    def sampleSphere(self):
        points = np.array([]).reshape(0, 2)
        angles = (np.linspace(0, 1, self.n_points, endpoint=False))
        shape = (self.n_points, 1)
        sin_phi = np.sin(2 * np.pi * angles[:]).reshape(shape)
        cos_phi = np.cos(2 * np.pi * angles[:]).reshape(shape)
        x = self.fac_r[0] * self.r * sin_phi + self.midpoint[0,0]
        y = self.fac_r[0] * self.r * cos_phi + self.midpoint[0,1]
        z = self.z(x, y)
        sphere_points = np.concatenate((x, y), axis=1)
        points = np.concatenate((points, sphere_points))
        return points

    def addParameter(self, parameter_name, parameter_value,
                     area_weighted=False):
        if parameter_name in self.parameter_names:
            index = self.parameter_names.index(parameter_name)
            self.parameter_values[index] = parameter_value
            self.parameter_area_weighted[index] = area_weighted
        else:
            self.parameter_values.append(parameter_value)
            self.parameter_names.append(parameter_name)
            self.parameter_area_weighted.append(area_weighted)

    def getTotalMeshArea(self):
        bounds = self.global_mesh.grid.GetBounds()
        r = self.r
        hx1 = max([- (self.midpoint[0][0] - r - bounds[0]), 0])
        hx2 = max([self.midpoint[0][0] + r - bounds[1], 0])
        hy1 = max([- (self.midpoint[0][1] - r - bounds[2]), 0])
        hy2 = max([self.midpoint[0][1] + r - bounds[3], 0])
        d11 = max([r - np.sqrt((r - hx1)**2 + (r - hy1)**2), 0])
        d21 = max([r - np.sqrt((r - hx2)**2 + (r - hy1)**2), 0])
        d22 = max([r - np.sqrt((r - hx2)**2 + (r - hy2)**2), 0])
        d12 = max([r - np.sqrt((r - hx1)**2 + (r - hy2)**2), 0])
        area = self.mesh.getMeshAreaForTriangles()
        #area += self.areaConeMinusTriangle(d11, d21, hy1)
        #area += self.areaConeMinusTriangle(d21, d22, hx2)
        #area += self.areaConeMinusTriangle(d22, d12, hy2)
        #area += self.areaConeMinusTriangle(d12, d11, hx1)
        return area

    def areaConeMinusTriangle(self, l1, l2, h):
        ## This function needs a drawing to be understood:
        # l1, l2 are the two lengths of the triangle
        # triangle c is pependicular to the radius of circle
        # diff_to_box corresponds to location of intersection of radius and
        # triangle_c
        # gamma is calculated using cosin_theorem
        r = self.r
        diff_to_box = (r-h)
        triangle_a = np.sqrt((r-l1)**2)
        triangle_b = np.sqrt((r-l2)**2)
        triangle_c = (np.sqrt(triangle_a**2-diff_to_box**2) +
                      np.sqrt(triangle_b**2-diff_to_box**2))
        gamma = np.arccos((triangle_a**2 + triangle_b**2 - triangle_c**2) /
                          (2 * triangle_a * triangle_b))
        area_cone = gamma * r**2/2.
        area_triangle = (r-h) * triangle_c / 2.
        return area_cone - area_triangle

    def addParameterToRootMesh(self, parameter_name):
        index = self.parameter_names.index(parameter_name)
        value = self.parameter_values[index]
        if self.parameter_area_weighted[index]:
            value = value/self.getTotalMeshArea()
        self.mesh.addPropertyVector(value, parameter_name, "double")

    def createMeshWithAllGlobalPoints(self, mesh_name):
        searchTree = MPF.MeshPointFinder(self.global_mesh.surface.grid)
        pointsTree = searchTree.findPointsWithinRadius(
                self.midpoint[0],
                self.r)

        self.mesh = MeshInteractor.MeshInteractor(mesh_name, z=self.global_mesh.z)
        self.mesh.CreateMeshFromPoints(pointsTree)
        ids = pointsTree[1]
        self.mesh.CreateMultipleTriangles()
        self.mesh_node_ids = ids

        for name in self.parameter_names:
            self.addParameterToRootMesh(name)

