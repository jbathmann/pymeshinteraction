#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@date: 2018-Today
@author: jasper.bathmann@ufz.de
"""

from MeshInteractor import MeshInteractor as MI
from PilotPoint import PilotPoint as PP

def function(x,y):
    return 0*x*y#-x/40. - y/2.

testmesh = MI("test")
testmesh.setMeshGeometry(30, 0, 9,
                         10, 0, 3, z = function, z_layers = 4)
pp = PP(.1, testmesh, x=1.5, y=1.5, z = function)


testmesh.addPilotPoint(pp)
pp.r = .1
testmesh.updateMesh()
#testmesh.addFirstLayer()
#testmesh.addMoreLayers()
#testmesh.readMesh("./testing/", "resample")
#testmesh.resampleDataset()
testmesh.outputMesh("./testing/")
