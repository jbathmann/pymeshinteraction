#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import MeshCreator as MC
import MeshPointFinder as MPF
import MeshEditor as ME
import numpy as np


class TreeBoundaryMesh:
    ## This class is designed to create a circular boundary mesh from a
    # location, a reference mesh and given radius. It also contains the
    # functionality to update/recreate the mesh, if the given radius has
    # changed. Additionally homogenous mesh properties can be added by one of
    # the build in functions.
    # @date: 2018 - Today
    # @author: jasper.bathmann@ufz.de
    def __init__(self, mesh, coordx, dx, coordy, dy, coordz, radius, treeName):
        self.rootRadius = radius
        self.bulkMesh = mesh
        self.coordx, self.coordy, self.coordz = coordx, coordy, coordz
        self.dx, self.dy = dx, dy
        self.pointFinder = MPF.MeshPointFinder(self.bulkMesh)
        self.pointsTree = self.pointFinder.findPointsWithinRadius(
                np.array([self.coordx, self.coordy, self.coordz]),
                self.rootRadius, self.dx, self.dy)
        self.treeId = str(treeName)
        self.treeMesh = MC.MeshCreator(self.treeName)
        self.treeMesh.CreateMeshFromPoints(self.pointsTree)
        self.editTree = ME.MeshEditor(self.treeMesh.pd)
        self.editTree.CreateMultipleTriangles()

    def updateTreeRadius(self, newRadius):
        self.rootRadius = newRadius
        self.pointsTree = self.pointFinder.findPointsWithinRadius(
                np.array([self.coordx, self.coordy, self.coordz]),
                self.rootRadius, self.dx, self.dy)
        self.treeMesh = MC.MeshCreator("Tree")
        self.treeMesh.CreateMeshFromPoints(self.pointsTree)
        self.editTree = ME.MeshEditor(self.treeMesh.pd)
        self.editTree.CreateMultipleTriangles()

    def configureTreeBoundaryCondition(self, psiName, gName, resiName):
        self.psiLeafName = psiName
        self.gName = gName
        self.resistanceName = resiName

    def createHomogenousTreeProperty(self, value, name, tYpe):
        self.editTree.AddHomogenousPropertyVector(value, name, tYpe)

    def outputMesh(self):
        self.treeMesh.OutputMesh()
