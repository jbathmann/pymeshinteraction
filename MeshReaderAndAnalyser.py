#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import vtk as vtk
import numpy as np


class MeshReaderAndAnalyser:
    ## Class which contains functionality to read vtu files. The most important
    # function is "getUnweightedPropertyArray().
    # @date: 2018 - Today
    # @author: jasper.bathmann@ufz.de

    def __init__(self, meshName):
        self.meshName = meshName

    def readMesh(self):
        self.meshReader = vtk.vtkXMLUnstructuredGridReader()
        self.meshReader.SetFileName(self.meshName)
        self.meshReader.Update()
        self.meshData = self.meshReader.GetOutput()
        self.points = self.meshData.GetPointData()

    def getUnweightedPropertyArray(self, propertyName):
        prop = self.points.GetArray(propertyName)
        valueNumber = prop.GetNumberOfTuples()
        valuearray = np.empty(valueNumber, dtype=float)
        for i in range(valueNumber):
                valuearray[i] = prop.GetTuple(i)[0]
        return valuearray
